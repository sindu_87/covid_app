import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class OptionsPage {
	
	WebDriver d;
	public OptionsPage(WebDriver originalDriver) {
		d=originalDriver;
		
		
	}
	
	@FindBy(xpath = "/html/body/div[1]/div/div/div/div/div[1]/h1")
	public WebElement tittle1;
	
	@FindBy(xpath = "/html/body/div[1]/div/section/div/div/section/div/div[1]/h3")
	public WebElement addedel;
	
	@FindBy(xpath = "/html/body/div[1]/div/section/div/div/section/div/div[2]/div/div/div/div/form/div[1]/h3")
	public WebElement Language;
	
	@FindBy(how = How.NAME, using = "lang_id")
	public WebElement slanguage;
	
	@FindBy(how = How.CLASS_NAME,using = "label-input100")
	public WebElement option;
	
	@FindBy(how = How.NAME, using = "source_id")
	public WebElement sourceid;
	
	@FindBy(xpath = "/html/body/div[1]/div/section/div/div/section/div/div[2]/div/div/div/div/form/div[3]/span[1]")
	public WebElement optionname;
	
	@FindBy(how = How.NAME, using = "source_name")
	public WebElement sourcename;
	


}

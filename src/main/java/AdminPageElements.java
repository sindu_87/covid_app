import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AdminPageElements {
	
	WebDriver d;
	public AdminPageElements(WebDriver originalDriver) {
		d=originalDriver;
		
		
	}
	
	@FindBy(xpath = "//*[@id=\"user\"]" )
	public WebElement username;
	
	@FindBy(xpath = "//*[@id=\"pass\"]")
	public WebElement password;
	
	@FindBy(xpath = "/html/body/div/div/form/span/button")
	public WebElement login;
	
	

}

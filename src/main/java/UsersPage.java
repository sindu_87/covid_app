import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class UsersPage {
	
	WebDriver d;
	public UsersPage(WebDriver originalDriver) {
		d=originalDriver;
		
		
	}
	
	@FindBy(xpath = "/html/body/div[1]/div/div/div/div/div[1]/h1")
	public WebElement tittle;
	
	@FindBy(xpath = "/html/body/div[1]/div/section/div/div/section/div/div[1]/h3")
	public WebElement viewusers;
	
	@FindBy(xpath = "/html/body/div[1]/div/section/div/div/section/div/div[2]/div/table/thead/tr/th[1]")
	public WebElement hash;
	
	@FindBy(xpath = "/html/body/div[1]/div/section/div/div/section/div/div[2]/div/table/thead/tr/th[2]")
	public WebElement fname;
	
	@FindBy(xpath = "/html/body/div[1]/div/section/div/div/section/div/div[2]/div/table/thead/tr/th[3]")
	public WebElement lname ;
	
	@FindBy(xpath = "/html/body/div[1]/div/section/div/div/section/div/div[2]/div/table/thead/tr/th[4]")
	public WebElement agegrp;
	
	@FindBy(xpath = "/html/body/div[1]/div/section/div/div/section/div/div[2]/div/table/thead/tr/th[5]")
	public WebElement gender;
	
	@FindBy(xpath = "/html/body/div[1]/div/section/div/div/section/div/div[2]/div/table/thead/tr/th[6]")
	public WebElement group;
	
	@FindBy(xpath = "/html/body/div[1]/div/section/div/div/section/div/div[2]/div/table/thead/tr/th[7]")
	public WebElement eid;
	
	@FindBy(xpath = "/html/body/div[1]/div/section/div/div/section/div/div[2]/div/table/thead/tr/th[8]")
	public WebElement source;
	
	@FindBy(xpath = "/html/body/div[1]/div/section/div/div/section/div/div[2]/div/table/thead/tr/th[9]")
	public WebElement datetime;

	
	

}

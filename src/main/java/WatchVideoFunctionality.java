import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class WatchVideoFunctionality {

    WebDriver d;

    public WatchVideoFunctionality(WebDriver originalDriver){  //this driver is coming from my test script
        d = originalDriver;
    }

    @FindBy(how = How.LINK_TEXT, using = "Watch Video")
    public WebElement watchVideoLink;

    @FindBy(how = How.XPATH, using = "//*[@id=\"ipage_2\"]/div/div")
    public WebElement confirmationModal;

    @FindBy(how = How.XPATH, using = "//*[@id=\"ipage_2\"]/div/div/div/form/div[2]/button")
    public WebElement yesButton;

    @FindBy(how = How.XPATH, using = "//*[@id=\"ipage_2\"]/div/div/div/form/div[2]/a")
    public WebElement noButton;

    @FindBy(how = How.XPATH, using = "//*[@id=\"ipage_2_thanks\"]/div/div")
    public WebElement noMessage;

    @FindBy(how = How.XPATH, using = "//*[@id=\"ipage_2_thanks\"]/div/div/div[1]/button/span")
    public WebElement noMessageCloseButton;

    @FindBy(how = How.XPATH, using = "//*[@id=\"ipage_2_1\"]/div/div")
    public WebElement yesForm;

    @FindBy(how = How.XPATH, using = "//*[@id=\"ipage_2_1\"]/div/div/div[2]/form/div[9]/button")
    public WebElement nextButton;

    @FindBy(how = How.ID_OR_NAME, using = "f2")
    public WebElement provinceSelector;

    @FindBy(how = How.XPATH, using = "//*[@id=\"f3\"]")
    public WebElement firstNameInput;

    @FindBy(how = How.ID_OR_NAME, using = "f4")
    public WebElement lastNameInput;

    @FindBy(how = How.ID_OR_NAME, using = "f5")
    public WebElement ageSelector;

    @FindBy(how = How.XPATH, using = "//*[@id=\"ipage_2_1\"]/div/div/div[2]/form/div[5]/div[1]/label")
    public WebElement sexSelector;

    @FindBy(how = How.XPATH, using = "//*[@id=\"ipage_2_1\"]/div/div/div[2]/form/div[6]/input[2]")
    public WebElement groupSelector;

    @FindBy(how = How.ID_OR_NAME, using = "f1")
    public WebElement emailId;

    @FindBy(how = How.ID_OR_NAME, using = "user_know")
    public WebElement hearAboutUs;

	@FindBy(xpath = "//*[@id=\"ipage_2_1\"]/div/div/div[2]/form/div[9]/button")
	public WebElement nextbutton;	
	
	@FindBy(xpath = "/html/body/section[2]/div[2]/div[1]/a/div")
	public WebElement outdoorSafetyVideo;
	

}
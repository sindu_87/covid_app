import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class Home {

    WebDriver d;

    public Home(WebDriver originalDriver){  //this driver is coming from my test script
        d = originalDriver;
    }

    @FindBy(how = How.LINK_TEXT, using = "Watch Video")
    public WebElement watchVideoLink;

    @FindBy(how = How.LINK_TEXT, using = "Download Training Modules in English (PDF)")
    public WebElement downloadLink;

}

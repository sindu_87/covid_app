import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;


public class ValidateWatchVideoTest {


    public static void main(String[] args) throws InterruptedException {
        int testNum = 0;

        System.setProperty("webdriver.chrome.driver", "D:\\Selenium\\chromedriver.exe");

        WebDriver driver = new ChromeDriver();
        driver.get("https://covid.delphitechnology.ca/scarb/index.php?lang_id=MQ==&lang_name=ZW5nbGlzaA==");

        Home home = PageFactory.initElements(driver, Home.class);

        WatchVideoFunctionality watchVideoFuntionality = PageFactory.initElements(driver, WatchVideoFunctionality.class);
        AdminPageElements adminpage = PageFactory.initElements(driver,AdminPageElements.class);
        UsersPage userpage = PageFactory.initElements(driver, UsersPage.class);
        OptionsPage optionp= PageFactory.initElements(driver, OptionsPage.class);
        

//        boolean isWatchVideoPresent = home.watchVideoLink.isDisplayed();
//
//        if (isWatchVideoPresent == true){
//            System.out.println("Test is passed");
//        }else{
//            System.out.println("Test is failed");
//        }

        //home.downloadLink.click();

        boolean isWatchVideoButtonPresent = watchVideoFuntionality.watchVideoLink.isDisplayed();
        if (isWatchVideoButtonPresent){
            System.out.println("Test " + ++testNum + " is passed");
        }else{
            System.out.println("Test " + ++testNum + " is failed");
        }

        // click on Watch Video button
        watchVideoFuntionality.watchVideoLink.click();
        Thread.sleep(1000);

        boolean confirmationModalPresent = watchVideoFuntionality.confirmationModal.isDisplayed();
        if (confirmationModalPresent){
            System.out.println("Test " + ++testNum + " is passed");
        }else{
            System.out.println("Test " + ++testNum + " is failed");
        }

        boolean isYesButtonPresent = watchVideoFuntionality.yesButton.isDisplayed();
        if (isYesButtonPresent){
            System.out.println("Test " + ++testNum + " is passed");
        }else{
            System.out.println("Test " + ++testNum + " is failed");
        }

        boolean isNoButtonPresent = watchVideoFuntionality.noButton.isDisplayed();
        if (isNoButtonPresent){
            System.out.println("Test " + ++testNum + " is passed");
        }else{
            System.out.println("Test " + ++testNum + " is failed");
        }

        // click on No button
        watchVideoFuntionality.noButton.click();
        Thread.sleep(1000);

        boolean noMessageShown = watchVideoFuntionality.noMessage.isDisplayed();
        if (noMessageShown){
            System.out.println("Test " + ++testNum + " is passed");
        }else{
            System.out.println("Test " + ++testNum + " is failed");
        }

        WatchVideoFunctionality wvf = PageFactory.initElements(driver, WatchVideoFunctionality.class);

        // close No message
        Thread.sleep(1000);
        watchVideoFuntionality.noMessageCloseButton.click();
        Thread.sleep(2000);

        // click on Watch Video button
        watchVideoFuntionality.watchVideoLink.click();
        Thread.sleep(1000);
        confirmationModalPresent = watchVideoFuntionality.confirmationModal.isDisplayed();
        if (confirmationModalPresent){
            System.out.println("Test " + ++testNum + " is passed");
        }else{
            System.out.println("Test " + ++testNum + " is failed");
        }

        // click on Yes button
        watchVideoFuntionality.yesButton.click();
        Thread.sleep(2000);

        boolean yesFormDisplayed = watchVideoFuntionality.yesForm.isDisplayed();
        if (yesFormDisplayed){
            System.out.println("Test " + ++testNum + " is passed");
        }else{
            System.out.println("Test " + ++testNum + " is failed");
        }

        // click on Yes button
        watchVideoFuntionality.nextButton.click();
        Thread.sleep(4000);

        yesFormDisplayed = watchVideoFuntionality.nextButton.isDisplayed();
        if (yesFormDisplayed){
            System.out.println("Test " + ++testNum + " is passed");
        }else{
            System.out.println("Test " + ++testNum + " is failed");
        }

        // set province
        Select s = new Select(watchVideoFuntionality.provinceSelector);
        s.selectByIndex(3);

        // set a first name
        watchVideoFuntionality.firstNameInput.sendKeys("San");
        // set a last name
        watchVideoFuntionality.lastNameInput.sendKeys("Jid");
        s = new Select(watchVideoFuntionality.ageSelector);
        s.selectByIndex(2);

        watchVideoFuntionality.sexSelector.click();
        watchVideoFuntionality.groupSelector.click();
        // set a Email ID
        watchVideoFuntionality.emailId.sendKeys("Jid@gmail.com");

        // How did you hear about us?
        s = new Select(watchVideoFuntionality.hearAboutUs);
        s.selectByIndex(4);

        // click on next button
        watchVideoFuntionality.nextButton.click();
        Thread.sleep(4000);
        yesFormDisplayed = watchVideoFuntionality.nextButton.isDisplayed();
        if (yesFormDisplayed){
            System.out.println("Test " + ++testNum + " is failed"); // even with all correct input the form doesn't go to the next page
        }else{
            System.out.println("Test " + ++testNum + " is passed");
        }

//NextButton
        
        watchVideoFuntionality.nextbutton.click();
        watchVideoFuntionality.outdoorSafetyVideo.click();
    }    

}

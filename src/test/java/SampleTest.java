import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class SampleTest {

    WebDriver driver;
    Home home;

    @BeforeTest
    void preRequisite(){
        //System.setproperty();
        driver = new ChromeDriver();
        driver.get("https://covid.delphitechnology.ca/scarb/index.php?lang_id=MQ==&lang_name=ZW5nbGlzaA==");
        home = PageFactory.initElements(driver, Home.class);
    }

    @Test
    void validateWatchVideoVisibility(){
        boolean isWatchVideoLinkPresent = home.watchVideoLink.isDisplayed();
        if (isWatchVideoLinkPresent ==  true){
            Reporter.log("Test is Passed");
        }
        else{
            Reporter.log("Test is failed");
        }
    }

    @AfterTest
    void clean(){
        driver.quit();
    }
}